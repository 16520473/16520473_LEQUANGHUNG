#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
    if (c1 != 0 && c2 != 0 && c1&c2 != 0)
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int check = CheckCase(c1, c2);
	while (check == 3)
	{
		ClippingCohenSutherland(r, P1, P2);
		c1 = Encode(r, P1);
		c1 = Encode(r, P2);
		check = CheckCase(c1, c2);
	}
	if (check == 2)
	{
		return 0;
	}
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	double m = float(dy) / dx;
	if (c1 == 0)
	{
		Vector2D C;
		C = P1;
		P1 = P2;
		P2 = C;
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
	}
	if (c1&LEFT != 0)
	{
		P1.y += m * (r.Left - P1.x);
		P1.x = r.Left;
		return;
	}
	if (c1&RIGHT != 0)
	{
		P1.y += m * (r.Right - P1.x);
		P1.x = r.Right;
		return;
	}
	if (c1&TOP != 0)
	{
		if (dx != 0)
		{
			P1.x += (r.Top - P1.y) / m;
		}
		P1.y = r.Top;
		return;
	}
	if (c1&BOTTOM != 0)
	{
		if (dx != 0)
		{
			P1.x += (r.Bottom - P1.y) / m;
		}
		P1.y = r.Bottom;
		return;
	}

}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int dx, dy, x1, x2, y1, y2, xmin, xmax, ymin, ymax;
	int a, b, c, d;
	float t1, t2;
	t1 = 0;
	t2 = 1;
	x1 = P1.x;
	x2 = P2.x;
	y1 = P1.y;
	y2 = P2.y;
	dx = x2 - x1;
	dy = y2 - y1;
	xmin = r.Left;
	xmax = r.Right;
	ymin = r.Top;
	ymax = r.Bottom;
	a = SolveNonLinearEquation(-dx, x1 - xmin, t1, t2);
	b = SolveNonLinearEquation(dx, xmax-x1, t1, t2);
	c = SolveNonLinearEquation( dy, ymax - y1, t1, t2);
	d = SolveNonLinearEquation(-dy, y1-ymin, t1, t2);
	if (a*b*c*d)
	{
		Q1.x = x1 + t1 * dx;
		Q1.y = y1 + t1 * dy;
		Q2.x = x1 + t2 * dx;
		Q2.y = y1 + t2 * dy;
		return 1;
	}
	return 0;

}
