#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3]; int y[3];
	double phi = M_PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 3;
	}
	for (int i = 0; i < 3; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3]);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4];
	int y[4];
	double phi = M_PI / 2;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 4;
	}
	for (int i = 0; i < 4; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4]);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	double phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5]);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8];
	int y[8];
	double phi = M_PI / 2;
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 8;
	}
	for (int i = 0; i < 8; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], x[(i + 1) %8], y[(i + 1) % 8]);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	double phi = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5]);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	int xp[5];
	int yp[5];
	double phi = M_PI / 2;
	double r = R * (sin(M_PI / 10) / sin(7 * M_PI / 10));
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + M_PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(phi + M_PI / 5) + 0.5);
		phi += (2 * M_PI) / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], xp[i], yp[i]);
		SDL_RenderDrawLine(ren, xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5]);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8];
	int y[8];
	int xp[8];
	int yp[8];
	double phi = M_PI / 2;
	double r = R * (sin(M_PI / 8) / sin(6 * M_PI / 8));
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + M_PI / 8) + 0.5);
		yp[i] = yc - int(r*sin(phi + M_PI / 8) + 0.5);
		phi += (2 * M_PI) / 8;
	}
	for (int i = 0; i < 8; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], xp[i], yp[i]);
		SDL_RenderDrawLine(ren, xp[i], yp[i], x[(i + 1) % 8], y[(i + 1) % 8]);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	int xp[5];
	int yp[5];
	double phi = startAngle;
	double r = R * (sin(M_PI / 10) / sin(7 * M_PI / 10));
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + M_PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(phi + M_PI / 5) + 0.5);
		phi += (2 * M_PI) / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		SDL_RenderDrawLine(ren, x[i], y[i], xp[i], yp[i]);
		SDL_RenderDrawLine(ren, xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5]);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	double q = r;
	double starAngle = M_PI / 2;
	while (q > 1)
	{
		DrawStarAngle(xc, yc, q, starAngle, ren);
		starAngle += M_PI;
		q = q * sin(M_PI / 10) / sin(7 * M_PI / 10);
	}

}
