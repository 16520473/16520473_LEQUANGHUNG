#include "FillColor.h"
#include <iostream>
using namespace std;

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
    SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

    Uint32 temp;
    Uint8 red, green, blue, alpha;

    /* Get Red component */
    temp = pixel & fmt->Rmask;  /* Isolate red component */
    temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
    red = (Uint8)temp;

    /* Get Green component */
    temp = pixel & fmt->Gmask;  /* Isolate green component */
    temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
    green = (Uint8)temp;

    /* Get Blue component */
    temp = pixel & fmt->Bmask;  /* Isolate blue component */
    temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
    blue = (Uint8)temp;

    /* Get Alpha component */
    temp = pixel & fmt->Amask;  /* Isolate alpha component */
    temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
    temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
    alpha = (Uint8)temp;

    SDL_Color color = {red, green, blue, alpha};
    return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				delete[] pixels;
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
			delete[] pixels;
		}
	}
	return infoSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
    if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
        return true;
    return false;
}

Uint32 get_pixel32(SDL_Surface *surface, int x, int y) 
{	
	Uint32 *pixels = (Uint32 *)surface->pixels; 
	return pixels[ ( y * surface->w ) + x ]; 
}

void put_pixel32(SDL_Surface *surface, int x, int y, Uint32 pixel) 
{ 
	Uint32 *pixels = (Uint32 *)surface->pixels; 
	pixels[ ( y * surface->w ) + x ] = pixel; 
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
                  SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int NhoNhat(int a, int b) {
	if (a < b)
		return a;
	return b;
}
int LonNhat(int a, int b) {
	if (a > b)
		return a;
	return b;
}
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if ((xc - x)*(xc - x) + (yc - y)*(yc - y) <= R*R)
		return true;
	return false;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int ymin = NhoNhat(y1, y2);
	int ymax = LonNhat(y1, y2);
	for (int y = ymin; y <= ymax; y++)
		if (isInsideCircle(xc, yc, R, x1, y) == true)
			SDL_RenderDrawPoint(ren, x1, y);
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int xnho = NhoNhat(vTopLeft.x, vBottomRight.x);
	int xlon = LonNhat(vTopLeft.x, vBottomRight.x);
	for (int x = xmin; x <= xmax; x++)
		FillIntersection(x, vTopLeft.y, x, vBottomRight.y, xc, yc, R, ren, fillColor);
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int xnho = NhoNhat(vTopLeft.x, vBottomRight.x);
	int xlon = LonNhat(vTopLeft.x, vBottomRight.x);
	int ynho = NhoNhat(vTopLeft.y, vBottomRight.y);
	int ylon = LonNhat(vTopLeft.y, vBottomRight.y);
	for (int x = xnho; x <= xlon; x++)
		Bresenham_Line(x, ynho, x, ylon, ren);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	Bresenham_Line(xc + x, yc + y, xc + x, yc - y, ren);
	Bresenham_Line(xc - x, yc + y, xc - x, yc - y, ren);
	Bresenham_Line(xc + y, yc + x, xc + y, yc - x, ren);
	Bresenham_Line(xc - y, yc + x, xc- y, yc - x, ren);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = R;
	int y = 0;
	int p = 3 - 2 * R;
	put4line(xc, yc, x, y, ren, fillColor);
	while (x > y) {
		if (p <= 0)
			p += 4 * y + 6;
		else {
			p += 4 * y - 4 * x + 10;
			x--;
		}
		y++;
		put4line(xc, yc, x, y, ren, fillColor);
	}
}

void Put2(int x, int y, int xcE, int ycE, int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor) {
	FillIntersection(xce + x, yce + y, xce + x, yce - y, xc, yc, R, ren, fillColor);
	FillIntersection(xce - x, yce + y, xce - x, yce - y, xc, yc, R, ren, fillColor);
}
void FillIntersectionEllipseCircle(int xcE, int ycE, int RE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = 0, y = b;
	float a4 = a*a;
	a4 *= a;
	a4 *= a;
	float z = a4 / (a*a + b*b);
	int p = -2 * a*a*b + a*a + 2 * b*b;
	Put2(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	while (x <= sqrt(z)) {
		if (p <= 0)
			p += 4 * b*b*x + 6 * b*b;
		else {
			p += 4 * b*b*x - 4 * a*a*y;
			y--;
		}
		x++;
		Put2(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	}
	// Area 2
	x = a, y = 0;
	p = b*b + 2 * a*a - 2 * a*b*b;
	Put2(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	while (x > sqrt(z)) {
		if (p <= 0)
			p += 4 * a*a*y + 6 * a*a;
		else {
			p += 4 * a*a*y - 4 * b*b*x;
			x--;
		}
		y++;
		Put2(x, y, xcE, ycE, xc, yc, R, ren, fillColor);
	}
}

void Put4(int x, int y, int xc1, int yc1, int xc2, int yc2, int R, SDL_Renderer *ren, SDL_Color fillColor) {
	FillIntersection(xc1 + x, yc1 + y, xc1 + x, yc1 - y, xc2, yc2, R, ren, fillColor);
	FillIntersection(xc1 - x, yc1 + y, xc2 - x, yc2 - y, xc2, yc2, R, ren, fillColor);
	FillIntersection(xc1 + y, yc1 + x, xc1 + y, yc1 - x, xc2, yc2, R, ren, fillColor);
	FillIntersection(xc1 - y, yc1 + x, xc1 - y, yc1 - x, xc2, yc2, R, ren, fillColor);
}
void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	int x = R1;
	int y = 0;
	int p = 3 - 2 * R1;
	Put4(x, y, xc1, yc1, xc2, yc2, R2, ren, fillColor);
	while (x > y) {
		if (p <= 0)
			p += 4 * y + 6;
		else {
			p += 4 * y - 4 * x + 10;
			x--;
		}
		y++;
		Put4(x, y, xc1, yc1, xc2, yc2, R2, ren, fillColor);
	}
}

